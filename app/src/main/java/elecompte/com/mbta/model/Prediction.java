
package elecompte.com.mbta.model;

import com.gustavofao.jsonapi.Models.Resource;

import java.util.ArrayList;
import java.util.List;


public class Prediction extends Resource {

    public String name;
    public List<Mode> mode = new ArrayList<>();
    public List<AlertHeader> alertHeaders = new ArrayList<>();

}
