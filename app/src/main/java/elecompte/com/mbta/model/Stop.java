
package elecompte.com.mbta.model;

import com.gustavofao.jsonapi.Models.Resource;

public class Stop extends Resource {

    public String name;
    public String parentStation;
    public String parentStationName;
    public String latitude;
    public String longitude;


    public Double getLatitude() {
        return Double.parseDouble(latitude);
    }
    public Double getLongitude() {
        return Double.parseDouble(longitude);
    }


    public String getName() {
        return name;
    }

    public String getParentStation() { return parentStation; }

    public String getParentStationName() { return parentStationName; }


}

