
package elecompte.com.mbta.model;

import com.gustavofao.jsonapi.Annotations.Type;
import com.gustavofao.jsonapi.Models.Resource;

@Type("routes")
public class Route extends Resource {

    private int type;
    private String short_name;
    private String long_name;

    public Route() {}

}
